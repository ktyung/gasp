a = ['  1 ', '2  ', '  3']
print(a)

strline = list()
intline = []

for line in a:
    print('lineb4 :',line)
    line = line.strip()
    print('lineafter strip',line)
    intline.append(int(line))
    strline.append(line)

print('strline:', strline)
print('intline:  ',intline)
print('maxint= ', max(intline))
print('maxstr ',max(strline))
print('this is the end of test!!')