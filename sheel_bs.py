
import requests
from bs4 import BeautifulSoup
import urllib.request
#import ssl
#ssl._create_default_https_context = ssl._create_unverified_context

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
url = "https://www.reddit.com/r/BabyYoda"
#headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) 
#AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'}

response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')
images = soup.find_all("img", attrs = {"alt": "Post image"})
number = 0

for image in images:
        image_src = image['src']
        print(image_src)
        urllib.request.urlretrieve(image_src, str(number))
        number += 1
