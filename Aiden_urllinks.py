# this is code from Aiden for web scraping project 2021/12/17/F

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

print('\n')
print('This run of urllinks.py starts here ---','\n')

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
url = 'https://vsgc.spacegrant.org/course/mod/assign/view.php?id=8921'
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
tags = soup('p')
for tag in tags:
    print(tags,'\n')
tagx = soup('a')
for tag in tagx:
    print(tag.get('href', None))

print('\n')

