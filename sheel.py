from bs4 import BeautifulSoup
import requests

url = 'https://www.python.org/Downloads/'
response = requests.get(url)

soup = BeautifulSoup(response.content, 'html.parser')

print(soup.prettify())

headers = soup.find('h1')
print(headers.text)

