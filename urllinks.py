# this is code from Aiden for web scraping project 2021/12/17/F

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

print('\n')
print('***** Anchor tags only--- urllinks.py starts here *****','\n')

# first set up url
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
url = 'https://secure.bankofamerica.com'

# now set the html 
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

"""
# do the <p>
tags = soup('p')
for tag in tags:
    print(tag,'\n')

"""
# do the <a>
tagx = soup('a')
for tag in tagx:
    print(tag.get('href', None))

print('***this run stops here***\n')

